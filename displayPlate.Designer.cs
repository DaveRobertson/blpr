﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace Player
{
    partial class displayPlate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public string LabelText
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public void TrafficLight(String theImage)
        {
            object O = Properties.Resources.ResourceManager.GetObject(theImage);
            trafficLight.Image = (Image)O;
            return;
        }
 

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.trafficLight = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.trafficLight)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("UKNumberPlate", 120F);
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(658, 113);
            this.label1.TabIndex = 0;
            this.label1.Text = "MA13 YHP";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // trafficLight
            // 
            this.trafficLight.Image = global::Player.Properties.Resources.TrafficLightGreen;
            this.trafficLight.Location = new System.Drawing.Point(805, 12);
            this.trafficLight.Name = "trafficLight";
            this.trafficLight.Size = new System.Drawing.Size(128, 171);
            this.trafficLight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.trafficLight.TabIndex = 1;
            this.trafficLight.TabStop = false;
            // 
            // displayPlate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(965, 212);
            this.Controls.Add(this.trafficLight);
            this.Controls.Add(this.label1);
            this.Name = "displayPlate";
            this.Text = "Plate Captured";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trafficLight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox trafficLight;
    }
}