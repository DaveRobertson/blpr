﻿/*
    SimpleLPR_CSharp

	Sample C# application demonstrating the usage of SimpleLPR
    Looks for license plates in all pictures in a folder an saves
    the output in a XML file.
 
	(c) Copyright Warelogic, 2009
	All rights reserved. Copying or other reproduction of this 
	program except for archival purposes is prohibited without
	written consent of Warelogic.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using SimpleLPR2;

namespace SimpleLPR_CSharp
{
    struct Result
    {
        public string fileName;
        public List<Candidate> lps;
    }

    class Program
    {
        private ISimpleLPR _lpr;                // Instance of the SimpleLPR engine
        private List<Result> _results;          // Recognition results
        private Stack<IProcessor> _processors;  // Pool of IProcessor, to avoid creating and destroying IProcessor on each detection
        private int _cPending;                  // Number of pending operations

        // Analyze a picture asynchronously.
        private void process(Object threadContext)
        {
            string imgFileName = (string)threadContext;  // The image file to be processed is supplied as the asynchronous call context

            // Get an IProcessor from the processor pool
            System.Threading.Monitor.Enter(this);
            IProcessor proc = _processors.Pop();
            System.Threading.Monitor.Exit(this);

            List<SimpleLPR2.Candidate> cds = proc.analyze(imgFileName, 120 /*maximum char height*/); // Look for license plates

            System.Threading.Monitor.Enter(this);

            _processors.Push(proc); // Return processor to the pool

            // Write result to the console
            System.IO.FileInfo ffo = new System.IO.FileInfo(imgFileName);
            Console.Write("{0} : ", ffo.Name);

            if (cds.Count == 0)
                Console.WriteLine("Nothing detected.");
            else
            {
                foreach (Candidate cd in cds)
                    Console.Write("[{0} --> {1}] ", cd.text, cd.confidence);

                Console.WriteLine();
            }

            Result res;
            res.fileName = imgFileName;
            res.lps = cds;
            _results.Add(res); // Keep result

            --_cPending;       // Decrement number of pending operations
            System.Threading.Monitor.Pulse(this);   // Signal end of operation
            System.Threading.Monitor.Exit(this);
        }

        void doIt( uint countryId, string srcFolder, string targetFile, string productKey )
        {
            _results.Clear();

            // Configure country weights based on the selected country

            if (countryId >= _lpr.numSupportedCountries)
                throw new Exception("Invalid country id");

            for (uint ui = 0; ui < _lpr.numSupportedCountries; ++ui)
                _lpr.set_countryWeight(ui, 0.0f);

            _lpr.set_countryWeight(countryId, 1.0f);

            _lpr.realizeCountryWeights();

            // Set the product key (if any)
            if (productKey != null)
                _lpr.set_productKey(productKey);

            // Initialize the pool of IProcessor
            _processors.Clear();
            for (int i = 0; i < Environment.ProcessorCount; ++i)
                _processors.Push(_lpr.createProcessor());

            // For each image in the source folder ... and sub folders
            System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(srcFolder);
            foreach (System.IO.FileInfo f in dInfo.GetFiles("*.*",System.IO.SearchOption.AllDirectories))
            {
                // Filter out non image files
                if ( f.Extension.ToLower() == ".jpg" || f.Extension.ToLower() == ".tif" || 
                     f.Extension.ToLower() == ".png" || f.Extension.ToLower() == ".bmp" ) 
                {
                    System.Threading.Monitor.Enter(this);
                    while (_cPending >= Environment.ProcessorCount) // Maximum number of simultaneous operations = number of processors 
                        System.Threading.Monitor.Wait(this);

                    ++_cPending;
                    // Execute plate recognition as an asynchronous operation through the process method
                    System.Threading.ThreadPool.QueueUserWorkItem(process, f.FullName);

                    System.Threading.Monitor.Exit(this);
                }
            }

            // Wait for all operations to complete
            System.Threading.Monitor.Enter(this);
            while (_cPending > 0)
                System.Threading.Monitor.Wait(this);
            System.Threading.Monitor.Exit(this);

            // Write out the results to a XML file
            XmlDocument xml = new XmlDocument();
            XmlProcessingInstruction basePI = xml.CreateProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xml.AppendChild(basePI);
            XmlElement baseElem = xml.CreateElement("results");
            xml.AppendChild(baseElem);

            foreach(Result res in _results)
            {
                XmlElement xmlRes = xml.CreateElement("result");
                baseElem.AppendChild(xmlRes);

                XmlElement xmlFile = xml.CreateElement("file");
                xmlRes.AppendChild(xmlFile);
                xmlFile.SetAttribute("path", res.fileName);

                XmlElement xmlCds = xml.CreateElement("candidates");
                xmlRes.AppendChild(xmlCds);

                foreach (Candidate cand in res.lps)
                {
                    XmlElement xmlCd = xml.CreateElement("candidate");
                    xmlCds.AppendChild(xmlCd);

                    xmlCd.SetAttribute("text", cand.text);
                    xmlCd.SetAttribute("country", cand.country);
                    xmlCd.SetAttribute("confidence", cand.confidence.ToString());
                    xmlCd.SetAttribute("lightBackground", cand.brightBackground.ToString());

                    foreach (Element elem in cand.elements)
                    {
                        XmlElement xmlElem = xml.CreateElement("element");
                        xmlCd.AppendChild(xmlElem);

                        xmlElem.SetAttribute("glyph", elem.glyph.ToString());
                        xmlElem.SetAttribute("confidence", elem.confidence.ToString());
                        xmlElem.SetAttribute("bbLeft", elem.bbox.Left.ToString());
                        xmlElem.SetAttribute("bbTop", elem.bbox.Top.ToString());
                        xmlElem.SetAttribute("bbWidth", elem.bbox.Width.ToString());
                        xmlElem.SetAttribute("bbHeight", elem.bbox.Height.ToString());
                    }
                }
            }

            xml.Save(targetFile);
        }

        Program( ISimpleLPR lpr )
        {
            _lpr = lpr;
            _processors = new Stack<IProcessor>();
            _results = new List<Result>();
        }

        static void Main(string[] args)
        {
            // Create an instance of the SimpleLPR engine
            ISimpleLPR lpr = SimpleLPR.Setup();

            // Output version number
            VersionNumber ver = lpr.versionNumber;
            Console.WriteLine("SimpleLPR version {0}.{1}.{2}.{3}", ver.A, ver.B, ver.C, ver.D);

            try
            {
                bool bOk = (args.Length == 3 || args.Length == 4);
                if (bOk)
                {
                    Program prg = new Program(lpr);
                    prg.doIt(uint.Parse(args[0]), args[1], args[2], args.Length == 4 ? args[3] : null);
                }
                else
                {
                    Console.WriteLine("\nUsage:  {0} <country id> <source folder> <target file> [product key]", Environment.GetCommandLineArgs()[0]);
                    Console.WriteLine("    Parameters:");
                    Console.WriteLine("     country id: Country identifier. The allowed values are");

                    for (uint ui = 0; ui < lpr.numSupportedCountries; ++ui)
                        Console.WriteLine("\t\t\t{0}\t: {1}", ui, lpr.get_countryCode(ui));

                    Console.WriteLine("     source folder: folder containing the images to be processed");
                    Console.WriteLine("     target file:   file where the output will be written");

                    Console.WriteLine("     product key:  product key file. this value is optional, if not provided SimpleLPR will run in evaluation mode");
                }  	
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Exception occurred!: {0}", ex.Message);
            }          
        }
    }
}
