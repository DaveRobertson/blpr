﻿/*
 * 
 * (C) Barclays 2015 - Class to load web data.  Can pull down data from mySQL DB - or use local Datatable XML compliant copy 
 * 
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Windows.Forms;

using MySql.Data.MySqlClient;


namespace Player
{
    class LoadWebData
    {
                        
        public static DataTable connectoToMySql(BLPRProperties props)
        {

          
            string connString = "SERVER=" +props.ServerName+";" +
                                "DATABASE=" + props.DatabaseName + ";" +
                                "UID=" + props.UserName + ";" +
                                "PASSWORD=" + props.Password + ";";
                
            DataTable dt = new DataTable();     // Data table to be returned

            try
            {
                MySqlConnection cnMySQL = new MySqlConnection(connString);

                MySqlCommand cmdMySQL = cnMySQL.CreateCommand();

                MySqlDataReader reader;

                cmdMySQL.CommandText = "select * from master";

                cnMySQL.Open();

                reader = cmdMySQL.ExecuteReader();

                dt.Load(reader);

                cnMySQL.Close();

                dt.WriteXml(props.DataTableFile, XmlWriteMode.WriteSchema);

                return dt;
            }
            catch (Exception e)
            {
                // Ok just load from the XML Version

                MessageBox.Show("Cannot load Web data - using local XML instead");
                try
                {
                    dt.ReadXml(props.DataTableFile);
                    return dt;
                }
                catch(Exception e1)
                {
                    MessageBox.Show("Cannot load local table: " + e1.Message);
                    System.Windows.Forms.Application.Exit();
                    
                }
                return null;

            }
        }
    }
}

