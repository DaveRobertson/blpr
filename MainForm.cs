﻿//
// Video and Plate Detection app
// 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;

using AForge.Video;
using AForge.Video.DirectShow;

namespace Player
{
    public partial class MainForm : Form
    {
        private Stopwatch stopWatch = null;
        private long frameCount=0;
        private displayPlate plateForm=null;
        private double confidenceLevel=0.85;
        private BLPRProperties props=new BLPRProperties();  // Create a general props data class
        private DataTable platedb;
        private int Distance;
        private String plateFound="8L ANK";
        private int plateResult = 0;

        // Class constructor
        public MainForm( )
        {
            InitializeComponent( );

            
        }

        private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            CloseCurrentVideoSource( );
        }

        // "Exit" menu item clicked
        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            
            //System.Windows.Forms.Application.Exit();


            plateForm.Close(); 
            this.Close();
            
            
        }

        // Open local video capture device
        private void localVideoCaptureDeviceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            VideoCaptureDeviceForm form = new VideoCaptureDeviceForm( );

            if ( form.ShowDialog( this ) == DialogResult.OK )
            {
                // create video source
                VideoCaptureDevice videoSource = form.VideoDevice;

                // open it
                OpenVideoSource( videoSource );
            }
        }

        // Open video file using DirectShow
        private void openVideofileusingDirectShowToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( openFileDialog.ShowDialog( ) == DialogResult.OK )
            {
                // create video source
                FileVideoSource fileSource = new FileVideoSource( openFileDialog.FileName );

                // open it
                OpenVideoSource( fileSource );
            }
        }

        // Open JPEG URL
        private void openJPEGURLToolStripMenuItem_Click( object sender, EventArgs e )
        {
            URLForm form = new URLForm( );

            form.Description = "Enter URL of an updating JPEG from a web camera:";
            form.URLs = new string[]
				{
					"http://195.243.185.195/axis-cgi/jpg/image.cgi?camera=1",
				};

            if ( form.ShowDialog( this ) == DialogResult.OK )
            {
                // create video source
                JPEGStream jpegSource = new JPEGStream( form.URL );

                // open it
                OpenVideoSource( jpegSource );
            }
        }

        // Open MJPEG URL
        private void openMJPEGURLToolStripMenuItem_Click( object sender, EventArgs e )
        {
            URLForm form = new URLForm( );

            form.Description = "Enter URL of an MJPEG video stream:";
            form.URLs = new string[]
				{
					"http://195.243.185.195/axis-cgi/mjpg/video.cgi?camera=4",
					"http://195.243.185.195/axis-cgi/mjpg/video.cgi?camera=3",
				};

            if ( form.ShowDialog( this ) == DialogResult.OK )
            {
                // create video source
                MJPEGStream mjpegSource = new MJPEGStream( form.URL );

                // open it
                OpenVideoSource( mjpegSource );
            }
        }

        // Capture 1st display in the system
        private void capture1stDisplayToolStripMenuItem_Click( object sender, EventArgs e )
        {
            OpenVideoSource( new ScreenCaptureStream( Screen.AllScreens[0].Bounds, 100 ) );
        }

        // Open video source
        private void OpenVideoSource( IVideoSource source )
        {
            // set busy cursor
            this.Cursor = Cursors.WaitCursor;

            // stop current video source
            CloseCurrentVideoSource( );

            // start new video source
            videoSourcePlayer.VideoSource = source;
            videoSourcePlayer.Start( );
            //videoSourcePlayer.Hide();

            // reset stop watch
            stopWatch = null;

            // start timer
            timer.Start( );

            this.Cursor = Cursors.Default;
        }

        // Close video source if it is running
        private void CloseCurrentVideoSource( )
        {
            if ( videoSourcePlayer.VideoSource != null )
            {

                videoSourcePlayer.VideoSource.SignalToStop();
                videoSourcePlayer.VideoSource.WaitForStop();
                videoSourcePlayer.VideoSource.Stop();

                videoSourcePlayer.SignalToStop( );
                videoSourcePlayer.WaitForStop();
                videoSourcePlayer.Stop();

                /*// wait ~ 3 seconds
                for ( int i = 0; i < 30; i++ )
                {
                    if ( !videoSourcePlayer.IsRunning )
                        break;
                    System.Threading.Thread.Sleep( 100 );
                }

                if ( videoSourcePlayer.IsRunning )
                {
                    videoSourcePlayer.Stop( );
                }
                 * */

                videoSourcePlayer.VideoSource = null;
            }
        }

        // New frame received by the player - we will do the LPR tests here
        private void videoSourcePlayer_NewFrame( object sender, ref Bitmap image )
        {
            DateTime now = DateTime.Now;
            Graphics g = Graphics.FromImage( image );

            Bitmap current = (Bitmap)image.Clone();
            string filepath = Environment.CurrentDirectory + "/frames/";
            string xmlPath = Environment.CurrentDirectory + "/results.xml";

            string ramdiskPath = props.RamDisk;
            string fileName = System.IO.Path.Combine(ramdiskPath, @"name.bmp");
            current.Save(fileName);

            // Start the engine and take a snap - use confidence level 
            
            String plate = LPREngine.startEngine(ramdiskPath, xmlPath, confidenceLevel);

            plateResult = 0;

            if (plate.Equals(""))
            {
                plateResult = 2;
            }
            else
            {
                plateResult = this.SearchPLate(plate,props.Distance);
            }

            plateFound = plate;

            current.Dispose();
            // paint current time
            SolidBrush brush = new SolidBrush( Color.Red );
            g.DrawString( now.ToString( ), this.Font, brush, new PointF( 5, 5 ) );
            brush.Dispose( );

            g.Dispose( );
            
            frameCount++;
        }

        // Search the plate database and return a value 
        private int SearchPLate(String plate,int Distance)
        {
            // Stage 1- enumaret each row and find a match

            try
            {
                // Trim it
                String plateTrim = plate.Replace(" ", string.Empty);
                //Console.WriteLine("Plate to check: " + plateTrim);
                //Console.WriteLine("Distance: "+ Distance);

                foreach (DataRow row in platedb.Rows)
                {
                    String regplate = row["REGplate"].ToString().Replace(" ", string.Empty); 

                    int result = LevenshteinDistance.Compute(plateTrim.Trim(), regplate.Trim());

                    //Console.WriteLine(result);
                    if (result < Distance)
                    {
                        //Console.Write("Result: "+ result.ToString() + "   ");
                        //Console.WriteLine(regplate.Trim() +" " + plate.Trim());
                        return 1;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return 0;




        }

        // On timer event - gather statistics
        private void timer_Tick( object sender, EventArgs e )
        {


            IVideoSource videoSource = videoSourcePlayer.VideoSource;

            if ( videoSource != null )
            {
                // get number of frames since the last timer tick
                int framesReceived = videoSource.FramesReceived;

                if ( stopWatch == null )
                {
                    stopWatch = new Stopwatch( );
                    stopWatch.Start( );
                }
                else
                {
                    stopWatch.Stop( );

                    float fps = 1000.0f * framesReceived / stopWatch.ElapsedMilliseconds;
                    fpsLabel.Text = fps.ToString( "F2" ) + " fps";

                    stopWatch.Reset( );
                    stopWatch.Start( );
                }
                //this.Invoke((MethodInvoker)delegate
                //{
                    plateForm.LabelText = plateFound;

                    label1.Text = frameCount.ToString() + " " + plateFound; ; // runs on UI thread

                    if (plateResult == 0)
                    {
                        plateForm.TrafficLight("TrafficLightRed");
                    }
                    if (plateResult == 1)
                    {
                        plateForm.TrafficLight("TrafficLightGreen");
                    }
                    if (plateResult == 2)
                    {
                        plateForm.TrafficLight("unknown");
                    }

                //});
            

            }



        }

        private void videoSourcePlayer_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void mainPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Load and create an LPR Engine we can use quickly
            //LPREngine 

            // Load the data table

            

            plateForm = new displayPlate();

            plateForm.Show();

            //load
            PropertiesFile config = new PropertiesFile("blpr.properties");
            props.loadFromFile(config);
            
            platedb = LoadWebData.connectoToMySql(props);

            if ( platedb == null)
            {
                MessageBox.Show("Cannot load database data - cannot search number plates", "ERROR", MessageBoxButtons.OK);

            }
            plateForm.BringToFront();

            //get value whith default value
            //String com_port = config.get("com_port", "1");

            //MessageBox.Show(com_port);
            //set value
          //  config.set("com_port", com_port);
        //save
            //config.Save();

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        public class ComboboxItem
        {
            public string Text { get; set; }
            public string Value { get; set; }
            public override string ToString() { return Text; }
        }


        private void confidence_SelectedIndexChanged(object sender, EventArgs e)
        {

            ComboBox cmb = (ComboBox)sender;
            int selectedIndex = cmb.SelectedIndex;
            String selectedValue = cmb.SelectedItem.ToString();

            //ComboboxItem selectedCar = (ComboboxItem)cmb.SelectedItem;

            //MessageBox.Show(String.Format("Index: [{0}] CarName={1}; Value={2}", selectedIndex, selectedCar.Text, selectedValue)); 

            confidenceLevel = Convert.ToDouble(selectedValue) / 100.0;
            props.Confidence = confidenceLevel;
            MessageBox.Show(String.Format("Select Item: [{0}] ", selectedValue)); 
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Load the properties file here
            propertiesForm pf = new propertiesForm(props);
            DialogResult dr = pf.ShowDialog();
            if (dr == DialogResult.OK)
            { 
                // Save the data to the file and reload  
            }
            
            pf = null;

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }
    }
}
